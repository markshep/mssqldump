# Overview

This script allows you to dump a database from a MS SQL Server.  It
works in much the same way that mysqldump does for MySQL databases.

# Dependencies

## All OSes

The script is written in Python and requires that the following are
installed:

- [python 2.7](http://www.python.org/download/releases/2.7/) - or a newer 2.x release
- [pyodbc](http://code.google.com/p/pyodbc/) - python bindings for the Open Database Connectivity API

## Linux

- [unixODBC](http://www.unixodbc.org/) - an implementation of the Open Database Connectivity API for Unix
- [FreeTDS](http://freetds.schemamania.org/) - low level libraries for talking to Sybase and MS SQL Server

You probably need to tell unixODBC about FreeTDS.  I did that on my
Debian box by running this command:

    odbcinst -i -d -f /usr/share/tdsodbc/odbcinst.ini

## Windows

Not tested on Windows yet!  Will probably need the relevant ODBC
driver installing.

# Author

Mark Sheppard <mark [AT] ddf [DOT] net>

# History

Started writing: September 2012
